package edu.tecnasa.ecommerce.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "E_CLAIM_TYPE")
public class ClaimType implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "CLAIM_TYPE")
	private String ClaimType;
	
	public String getClaimType() {
		return ClaimType;
	}

	public void setClaimType(String clamType) {
		ClaimType = clamType;
	}
	
	

}
