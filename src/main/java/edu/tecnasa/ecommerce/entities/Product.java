package edu.tecnasa.ecommerce.entities;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "E_PRODUCT")
public class Product  implements Identifiable, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_PRODUCT")
	@JsonProperty("Id")
	private Long id;
	
	@Column(length = 50, nullable = false)
	@JsonProperty("Title")
	private String title;
	
	@Column(nullable = false, precision = 19, scale = 2)
	@JsonProperty("Price")
	private BigDecimal price;
	
	@Column(nullable = false)
	@JsonProperty("IsSpecial")
	private boolean special;
	
	@Column(length = 255)
	@JsonProperty("Desc")
	private String description;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_CATEGORY", nullable = false)
	private Category category;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public boolean isSpecial() {
		return special;
	}
	public void setSpecial(boolean special) {
		this.special = special;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	
	
}