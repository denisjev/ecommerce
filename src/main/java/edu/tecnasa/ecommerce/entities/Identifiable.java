package edu.tecnasa.ecommerce.entities;

public interface Identifiable {
	public Long getId();
}
