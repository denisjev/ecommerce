package edu.tecnasa.ecommerce.jsf;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.collections4.IterableUtils;

import edu.tecnasa.ecommerce.dao.CategoryDao;
import edu.tecnasa.ecommerce.entities.Category;

@Named("categoriesViewBean")
@ViewScoped
public class CategoriesViewBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private CategoryDao categoryDao;
	
	private List<Category> categories;

	public List<Category> getCategories() {
		if(categories == null)
			categories = IterableUtils.toList(categoryDao.findAll());
		return categories;
	}

	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}
}
