package edu.tecnasa.ecommerce.jsf;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.omnifaces.util.Faces;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import edu.tecnasa.ecommerce.dao.*;
import edu.tecnasa.ecommerce.entities.*;
import edu.tecnasa.ecommerce.errors.EcommerceGeneralException;

@Named("productsFormBean")
@ViewScoped
public class ProductsFormBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private ProductDao productDao;
	
	@Inject
	private CategoryDao categoryDao;
	
	@Inject
	private UserDao userDao;
	
	List<Category> categories = null;
	private Product product;
	
	@PostConstruct
	public void init() {
		Long id = NumberUtils.toLong(Faces.getRequestParameter("id"));
		
		if(id <= 0)
			product = new Product();
		else
			product = productDao.findById(id).get();
	}
	
	public void delete() {
		if(product == null)
			throw new EcommerceGeneralException();
		
		String userName = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if(principal instanceof UserDetails)
			userName = ((UserDetails) principal).getUsername();
		else
			userName = principal.toString();
		
		boolean canSaveProduct = false;
		User currentUser = userDao.findByUserName(userName);
		for(ClaimType ct : currentUser.getClaimTypes())
			if("CanSaveProduct".equals(ct.getClaimType()))
			{
				canSaveProduct = true;
				break;
			}
		
		if(!canSaveProduct)
			throw new EcommerceGeneralException("No permission!");
		
		productDao.delete(product);
		
		try {
			Faces.redirect("/index.xhtml");
		}catch(Exception e)	{
			throw new EcommerceGeneralException(e.getMessage(), e);
		}
	}
	
	public void save() {
		if(product == null)
			throw new EcommerceGeneralException();
		
		product = productDao.save(product);
		
		try {
			Faces.redirect("/index.xhtml");
		}catch(Exception e)	{
			throw new EcommerceGeneralException(e.getMessage(), e);
		}
	}

	public List<Category> getCategories() {
		if(categories == null)
			categories = IterableUtils.toList(categoryDao.findAll());

		return categories;
	}

	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
}
