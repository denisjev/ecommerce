package edu.tecnasa.ecommerce.jsf;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.lang3.math.NumberUtils;
import org.omnifaces.util.Faces;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import edu.tecnasa.ecommerce.dao.*;
import edu.tecnasa.ecommerce.entities.*;
import edu.tecnasa.ecommerce.errors.EcommerceGeneralException;

@Named("categoriesFormBean")
@ViewScoped
public class CategoriesFormBean implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Inject
	private CategoryDao categoryDao;

	@Inject
	private UserDao userDao;
	
	private Category category;

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}
	
	@PostConstruct
	public void init() {
		Long id = NumberUtils.toLong(Faces.getRequestParameter("id"));
		
		if(id <= 0)
			category = new Category();
		else
			category = categoryDao.findById(id).get();
	}
	
	public void delete() {
		
		if(category == null)
			throw new EcommerceGeneralException();
		
		String userName = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if(principal instanceof UserDetails)
			userName = ((UserDetails) principal).getUsername();
		else
			userName = principal.toString();
		
		boolean canSaveCategory = false;
		User currentUser = userDao.findByUserName(userName);
		for(ClaimType ct : currentUser.getClaimTypes())
			if("CanSaveCategory".equals(ct.getClaimType()))
			{
				canSaveCategory = true;
				break;
			}
		
		if(!canSaveCategory)
			throw new EcommerceGeneralException("No permission!");
		
		categoryDao.delete(category);
		
		try {
			Faces.redirect("/categories.xhtml");
		}catch(Exception e)	{
			throw new EcommerceGeneralException(e.getMessage(), e);
		}
	}
	
	public void save() {
		if(category == null)
			throw new EcommerceGeneralException();
		
		category = categoryDao.save(category);
		
		try {
			Faces.redirect("/categories.xhtml");
		}catch(Exception e)	{
			throw new EcommerceGeneralException(e.getMessage(), e);
		}
	}
}
