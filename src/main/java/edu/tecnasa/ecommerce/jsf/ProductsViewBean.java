package edu.tecnasa.ecommerce.jsf;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.collections4.IterableUtils;

import edu.tecnasa.ecommerce.dao.ProductDao;
import edu.tecnasa.ecommerce.entities.*;

@Named("productsViewBean")
@ViewScoped
public class ProductsViewBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private ProductDao productDao;
	
	private List<Product> products;

	public List<Product> getProducts() {
		if(products == null)
			products = IterableUtils.toList(productDao.findAll());
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}
}
